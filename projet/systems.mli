(** Words, rewrite systems, and rewriting *)
open Turtle
type 's word =
  | Symb of 's
  | Seq of 's word list
  | Branch of 's word

type 's rewrite_rules = 's -> 's word

type 's system = {
    axiom : 's word;
    rules : 's rewrite_rules;
    interp : 's -> Turtle.command list }

(** Put here any type and function interfaces concerning systems *)
val iterate: 'a word -> ('a -> 'a word) ->('a word)
val iterate_word_n : 'a word -> ('a -> 'a word) -> int -> int -> 'a word 
val iteration : 'a system -> int -> 'a word
val interp_word : 'a word -> ('a -> command list) -> command list
val read_file_send_trio: in_channel -> int -> (string* string list * string list) -> (string * string list *string list)
val transforme_string: string ->  char word
val get_axiom_string: (string*string list*string list)-> string
val get_rules_string: (string*string list*string list)-> string list
val get_interp_string: (string*string list*string list)-> string list
val new_tuples: string list -> (char * string)list
val new_interp:string->(char ->Turtle.command list)
val system_read : string -> char system 
