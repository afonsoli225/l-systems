open Stack 

(** Turtle graphical commands *)
type command =
| Line of int      (** advance turtle while drawing *)
| Move of int      (** advance without drawing *)
| Turn of int      (** turn turtle by n degrees *)
| Store            (** save the current position of the turtle *)
| Restore          (** restore the last saved position not yet restored *)

(** Position and angle of the turtle *)
type position = {
  x: float;        (** position x *)
  y: float;        (** position y *)
  a: int;          (** angle of the direction *)
}

(** Put here any type and function signatures concerning turtle *)

val turn : position -> int -> position
val move : position -> int -> position
val line : position -> int -> position

val launch : command -> position t * position -> int -> position t * position
val mypush : position -> position t -> position t
val mypop : position t * position -> position t * position
val parcours  : command list -> int -> position t * position
val parcours_aux: command list -> position t * position -> int -> position t * position
