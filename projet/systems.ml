(** Words, rewrite systems, and rewriting *)
open Turtle
type 's word =
  | Symb of 's
  | Seq of 's word list
  | Branch of 's word

type 's rewrite_rules = 's -> 's word

type 's system = {
    axiom : 's word;
    rules : 's rewrite_rules;
    interp : 's -> Turtle.command list }


(** Put here any type and function implementations concerning systems *)
(*iterer le mot tenant compte des regles du systeme*)
let rec iterate (word:'a word) (rules:('a ->'a word)):('a word) = 
  let rec iterate_l l rules =
    match l with
    | [] -> failwith "Empty list"
    | [x] -> [iterate x rules]
    | x::xs -> (iterate x rules)::(iterate_l xs rules)
  in
  match word with
  | Symb s -> rules s
  | Seq l -> Seq (iterate_l l rules)
  | Branch b -> Branch (iterate b rules)
;;
(*iteration recurive de taille k jusqua n*)
let rec iterate_word_n (word:'a word) (rules:('a ->'a word)) (k :int) (n :int) : ('a word) = 
  if(k < n) then
    iterate_word_n (iterate word rules) (rules) (k + 1) n
  else 
    word
;;
(*iterer le systeme*)
let iteration (system: 'a system) (n:int):('a word) = 
  iterate_word_n (system.axiom) (system.rules) 0 n
;;
(*iterer le mot avec interpretation*)
let rec interp_word (word:'a word) (interp:('a -> command list)):(command list) =
  let rec interp_word_list l =
    match l with
    | [] -> failwith "Empty list"
    | [x] -> interp_word x interp
    | x::xs -> List.append (interp_word x interp) (interp_word_list xs)
  in
  match word with
  | Symb s -> interp s
  | Seq l -> interp_word_list l
  | Branch b -> Store::(List.append (interp_word b interp) ([Restore]))
;;
(*Lecture du fichier et renvoyer un triplet*)
let rec read_file_send_trio (file: in_channel) (number:int)(trio:string * string list *string list) : (string * string list *string list) = 
   
   let (a,b,c)= trio in 
   try
      let line = input_line file in
      if line = "" then
         read_file_send_trio file (number+1) (a,b,c)
      else
         if String.get line 0 = '#' then 
            read_file_send_trio file number (a,b,c)
         else
            if(number=1) then 
            read_file_send_trio file number ((line),b,c)
            else if(number=2) then 
            read_file_send_trio file number (a,line::b,c) 
          else
          read_file_send_trio file number (a,b,line::c) 
   with End_of_file -> (a,b,c)  
;;
let transforme_string (str:string) :('s word)=
  if (String.length str = 1) then Symb str.[0]
  else (
    let rec transforme_string_aux str n branch i = 
      if (String.length str = n) then []
      else(
        match str.[n] with
        | '[' -> transforme_string_aux str (n+1) true (n+1) 
        | ']' -> 
          let list_branch = (transforme_string_aux (String.sub str i (n-i)) 0 false i) in
          if (List.length list_branch = 1) then 
            (Branch (List.hd list_branch))::(transforme_string_aux str (n+1) false 0)
          else 
            (Branch (Seq list_branch)) ::(transforme_string_aux str (n+1) false 0)
        | x -> 
          if (branch = true) then 
            transforme_string_aux str (n+1) branch i
          else 
            (Symb x)::(transforme_string_aux str (n+1) branch i)
      )
    in
    let word_list = transforme_string_aux str 0 false 0 in
    if (List.length word_list = 1) then List.hd word_list
    else Seq word_list
  )
;;

(*getteurs*)
let get_axiom_string  (a,b,c)=
  a
;;
let get_rules_string  (a,b,c)=
  b
;;
let get_interp_string (a,b,c)=
  c
;;

(*creer une liste de tuples avec conditio partant d'une liste*)
let rec new_tuples (l: 's list): (('a * 's)list)=
  match l with
  | []->[]
  | c::ts ->
    let res=String.split_on_char ' ' c in 
      if List.length res=1 then []
    else [((List.nth res 0).[0],List.nth res 1)]@(new_tuples ts)
;;
(*appliquer une fonction sur une liste de couples*)
let associate_function action tuples=
  fun s-> 
  begin 
    match List.assoc_opt s tuples with
    | None -> action s
    | Some x -> x
  end
;;
(*Partant d'un unique et renvoyer la fonction interpretation*)
let new_interp (s:string):('s ->Turtle.command list)=
  let i_list=String.split_on_char ':' s in
  let i_tuple=new_tuples i_list in

  let modify(a,b)=
    let rest=String.sub b 1 ((String.length b)-1) in 
    let dist= int_of_string rest in 
    match String.get b 0 with
    | 'L' -> (a,[Turtle.Line dist])
    | 'M' -> (a,[Turtle.Move dist])
    | 'T' -> (a,[Turtle.Turn dist])
    |  _  -> failwith "not found in dictionary"
  in

  let paires= List.map modify i_tuple in

  let action(s:char):(Turtle.command list)=
  match s with
  | '[' -> [Turtle.Store]
  | ']' -> [Turtle.Restore]
  |  _  -> failwith "error"
  in

  associate_function action paires
;;
(*lecture du fichier et construction d'un systeme*)
let system_read(str: string) : 's system =
    try 
    
      let ic = open_in (str) in (*Ouverture du ficher*)
      let (ax,rul,inter)= read_file_send_trio ic 1 ("" ,[],[]) in (*Lecture du fichier et recuperer les donnees*)
      
      let axio  = transforme_string( get_axiom_string((ax,rul,inter))) in (*Recuperer l'axiome*)
      
      let b_list= get_rules_string((ax,rul,inter)) in (*Recuperer les regles*)
      
      let c_list= get_interp_string((ax,rul,inter)) in (*Recuperer les interpretation*)
      
      let c_string=String.concat ":" c_list in
      (*construction des regles en regardant la liste de string*)
      let rec rules_aux (s: 's) (rl : string list) : 's word =
         match rl with
         |[]       -> Symb s 
         |a::t -> if (a.[0] = s && a.[2]!=' ') then
                      transforme_string(String.sub a 2 ((String.length a)-2))
                     else
                        match s with 
                        | e -> rules_aux s t
      in
      (*construction des regles *)
      let rules_function (s: 's) : ('s word) =
         rules_aux s b_list
      in
      close_in (ic);
      {  (*insertions des valeurs et fonctions dans notre systeme*)
         axiom = axio;
         rules = rules_function;
         interp = new_interp c_string
      }

   with Sys_error (open_in) ->failwith "out"
;;






