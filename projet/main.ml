open Lsystems (* Librairie regroupant le reste du code. Cf. fichier dune *)
open Systems (* Par exemple *)
open Graphics
open Turtle
open Examples
open Stack

(** Gestion des arguments de la ligne de commande.
    Nous suggérons l'utilisation du module Arg
    http://caml.inria.fr/pub/docs/manual-ocaml/libref/Arg.html
*)

let usage = (* Entete du message d'aide pour --help *)
  "Interpretation de L-systemes et dessins fractals"

let action_what () = Printf.printf "%s\n" usage; exit 0

let cmdline_options = [
("--what" , Arg.Unit action_what, "description");
]

let extra_arg_action = fun s -> failwith ("Argument inconnu :"^s)

(*notre main*)
let main () =
  
  auto_synchronize false;; (*Graphics standard*)
  display_mode false;;
  remember_mode true;;
  open_graph (Printf.sprintf " %ix%i" 1500 900);;
  synchronize ();;
  let i=ref 1 ;;(*variable d'iteration*)
  while(not false) do (*tant que c'est pas faux, construisons notre systeme*)
    let sys=system_read Sys.argv.(1) in (*scanner*)
    let word1 : 'a word = iteration sys !i in (*mot après i iteration*)
    let (pile,pos)= parcours(interp_word word1 sys.interp) !i in 
    let s = Graphics.wait_next_event [Graphics.Button_down; Graphics.Key_pressed] in (*Evenements avec boutons et cles*)
    if s.Graphics.button (*le mot va changer, du coup on dessine à nouveau*)
      then begin
        i:=!i+1;
        clear_graph();(*nettoyer le graph*)
      end
    else if s.key =='q' then exit 0 (*sortir de notre programme*)
              
      
  done
;;
  

  (*Arg.parse cmdline_options extra_arg_action usage;*)
 


(** On ne lance ce main que dans le cas d'un programme autonome
    (c'est-à-dire que l'on est pas dans un "toplevel" ocaml interactif).
    Sinon c'est au programmeur de lancer ce qu'il veut *)

let () = if not !Sys.interactive then main ()

